# Gergely Noémi-Laura
# gnim1720

import sys

def build_from_file(file):
    edges = []
    costs = []
    with open(file,'r') as f:
        n = int(f.readline())
        for a in f.readlines():
            elem = a.split()
            tup = (int(elem[0]), int(elem[1]))
            edges.append(tup)
            costs.append(int(elem[2]))

    return n, edges, costs

def cluster(n, edges, costs):

    vertices = [i+1 for i in range(n)]

    k = 4

    minimum_cost = 0

    clusters = n

    while clusters >= k:
        minimum_position = costs.index(min(costs))
        minimum_cost = costs[minimum_position]
        del costs[minimum_position]

        vertice_tuple = edges[minimum_position]
        del edges[minimum_position]

        a = vertice_tuple[0]
        b = vertice_tuple[1]
        value1 = vertices[a-1]
        value2 = vertices[b-1]
        
        if value1 != value2:
            clusters -= 1
            for i in range(n):
                if vertices[i] == value1:
                     vertices[i] = value2

    return minimum_cost

def main():
    if len(sys.argv) != 2:
        print('Invalid argument!')
        sys.exit(1)

    n, edges, costs = build_from_file(sys.argv[1])

    minimum_distance = cluster(n, edges, costs)

    print(minimum_distance)

if __name__ == '__main__':
    main()