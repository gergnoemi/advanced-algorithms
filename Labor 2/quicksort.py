import sys

def build_from_file(file):
    x = []
    
    with open(file, "r") as f:
        x.extend([int(a.strip()) for a in f.readlines()])
    
    return x

def split(x, l, r, index):
    a = x[index]
    x[index] = x[l]
    x[l] = a
    p = x[l]
    i = (l + 1)

    for j in range(l + 1, r + 1):
        if x[j] < p:
            a = x[j]
            x[j] = x[i]
            x[i] = a
            i += 1

    a = x[l]
    x[l] = x[i-1]
    x[i-1] = a
    
    return i - 1

def median(x, l, r):
    m = x[(r - l) // 2 + l]

    y = []
    y.extend([x[l], x[r], m])
    y.sort()
    
    if y[1] == x[l]:
        return l
    if y[1] == x[r]:
        return r
    return ((r - l) // 2 + l)

def quicksort(x, l, r):
    
    c1 = 0
    c2 = 0
    if (l < r):
        #left
        #s = split(x, l, r, l)

        #right
        #s = split(x, l, r, r)

        #median
        m = median(x, l, r)
        s = split(x, l, r, m)

        c1 = quicksort(x, l, s-1)
        c2 = quicksort(x, s+1, r)
        counter = r - l + c1 + c2
        return counter
    else:
        return 0

def main():
    
    if len(sys.argv) != 2:
        print("Invalid argument")
        sys.exit(1)
        
    x = build_from_file(sys.argv[1])

    #------------quicksort-------------
    counter = quicksort(x,0,9999)
    print("Number of comparisons:", counter)

if __name__ == "__main__":
    main()