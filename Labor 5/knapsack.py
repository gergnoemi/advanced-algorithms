# Gergely Noémi-Laura
# gnim1720

import sys

def build_from_file(file):
    x = []
    f = open(file, 'r')
    input = f.readline()
    words = input.split()
    cap = int(words[0])
    n = int(words[1])
    with f:
        x.extend([tuple(a.split()) for a in f.readlines()])

    v = []
    w = []
    for t in x:
        v.append(int(t[0]))
        w.append(int(t[1]))

    return cap, n, v, w

def knapsack(cap, n, v, w):
    past = []
    current = []
    for i in range(0, cap+1):
        past.append(0)
        current.append(0)
    
    step = 0
    while step < n:
        weight = w[step]
        value = v[step]
        
        for i in range(0, cap+1):
            if weight <= i:
                first_value = past[i - weight] + value
                second_value = past[i]
                if first_value > second_value:
                    current[i] = first_value
                else:
                    current[i] = second_value
            else:
                current[i] = past[i]

        for i in range(0, cap+1):
            past[i] = current[i]
            current[i] = 0

        step += 1

    return past[cap]

def main():
    if len(sys.argv) != 2:
        print("Invalid argument")
        sys.exit(1)
        
    cap, n, v, w = build_from_file(sys.argv[1])
    #cap = 6
    #n = 4
    #v = [3, 2, 4, 4]
    #w = [4, 3, 2, 3]

    result = knapsack(cap, n, v, w)
    print(result)

if __name__ == '__main__':
    main()