import sys
import random

a = random.randint(1,1000)
b = random.randint(1,1000)
c = random.randint(1,1000)
d = random.randint(1,1000)

def build_from_file(file):
    x = []
    f = open(file, 'r')
    n = f.read()
    k = f.read()
    with f:
        x.extend([str(a.strip()) for a in f.readlines()])

    return n, k, x

def hash_function(key):
    x = key.split('.')

    result = ( a * int(float(x[0])) + b * int(float(x[1])) + c * int(float(x[2])) + d * int(float(x[3])) ) % 5000

    return result

class Node:

    def __init__(self, key, value):
        self.key = key
        self.value = value

class hash_map:

    def __init__(self):
        self.size = 0
        self.map = [[] for i in range(5000)]

    def put(self, key, value):
        node = Node(key, value)
        hash_code = hash_function(key)
        if not self.map[hash_code]:
            self.map[hash_code] = [node]
            self.size += 1
        else:
            current_list = self.map[hash_code]
            for i in current_list:
                if i.key == key:
                    i.value += 1
                    return
            current_list.append(node)
            self.size += 1
    
    def get(self, key):
        hash_code = hash_function(key)
        for i in self.map[hash_code]:
            if i.key == key:
                return i.value
        return 'Key doesn\'t exist'

    def __len__(self):
        return self.size

    def number_of_frequent(self, k):
        count = 0
        
        for l in self.map:
            for i in l:
                if i.value >= k:
                    count += 1

        return count

def main():

    if len(sys.argv) != 2:
        print("Invalid argument")
        sys.exit(1)
        
    n, k, x = build_from_file(sys.argv[1])

    hashmap = hash_map()
    for i in x:
        hashmap.put(i, 1)
    print(len(hashmap))
    print(hashmap.number_of_frequent(int(k)))
    
if __name__ == '__main__':
    main()