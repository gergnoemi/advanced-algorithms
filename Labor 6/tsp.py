# Gergely Noémi-Laura
# gnim1720

import sys
import math
from math import sqrt
import itertools

def build_from_file(file):
    xs = []
    ys = []
    f = open(file, 'r')
    n = int(f.readline())
    l = []

    with f:
       l.extend([tuple(a.split()) for a in f.readlines()])

    for i in l:
        xs.append(float(i[0]))
        ys.append(float(i[1]))

    return n, xs, ys

def euclidean(x1, y1, x2, y2):
    sum = pow((x1 - x2), 2) + pow((y1 - y2), 2)
    return sqrt(sum)

def tsp(n, xs, ys):

    #vertices from 2 to n
    vertices = [i for i in range(2,n+1)]
    A = {}
    row = []

    for i in range(1,n):
        
        #we are making the subsets: combinations of the vertices
        for subset in itertools.combinations(vertices, i):
            S = [1]
            S = S + list(subset)
            # print(S)

            row = [0 for i in range(n-1)]
            for e in S:
                if (e != 1):
                    Ssub = S.copy()
                    Ssub.remove(e)
                    if len(Ssub) > 1:
                        rowCopy = A[tuple(Ssub)].copy()
                        min = 1111111
                        for j in range(n-1):
                            if (j+2 != e and rowCopy[j] != 0):
                                sum = rowCopy[j] + euclidean(xs[j+1], ys[j+1], xs[e-1], ys[e-1])
                                if (sum < min):
                                    min = sum
                        
                        row[e-2] = min
                    else:
                        row[e-2] = euclidean(xs[0], ys[0], xs[e-1], ys[e-1])

            A[tuple(S)] = row.copy()

    result = row.copy()
    for i in range(n-1):
        result[i] = result[i] + euclidean(xs[0], ys[0], xs[i+1], ys[i+1])
    print(result)

def main():
    if len(sys.argv) != 2:
        print("Invalid argument")
        sys.exit(1)

    n, xs, ys = build_from_file(sys.argv[1])

    result = tsp(n, xs, ys)

if __name__ == '__main__':
    main()
